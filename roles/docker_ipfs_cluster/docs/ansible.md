<!-- markdownlint-disable -->
## Role Variables

| Variable     | Default Value  | Description  |
| ------------ | -------------- | ------------ |
| ipfs_cluster_data_dir | `/var/lib/ipfs` | Filesystem path for the IPFS service data directory. |
| ipfs_cluster_data_dir_mode | `"0750"` | Filesystem permissions to set for the IPFS service data directory. |
| ipfs_cluster_docker_network | `ipfs` | Name of the Docker network to be created and used by the IPFS services. |
| ipfs_cluster_restart | `true` | Whether or not to restart the IPFS services as part of the playbook run. |
| ipfs_cluster_user_gid | ~ | GID of the Unix group to be created and used by the IPFS services. Leave as null to allow for the next available GID to be selected by the system. |
| ipfs_cluster_user_groupname | `ipfs` | The name of the Unix group to be created and used by the IPFS services. |
| ipfs_cluster_user_uid | ~ | UID of the Unix user to be created and used by the IPFS services. Leave as null to allow for the next available UID to be selected by the system. |
| ipfs_cluster_user_username | `ipfs` | The name of the Unix user to be created and used by the IPFS services. |
<!-- markdownlint-enable -->
